#!/usr/bin/env bash
set -Eeuo pipefail

cd "$(dirname "$(readlink -f "$BASH_SOURCE")")"

versions=( "$@" )

generated_warning() {
    cat <<EOH
#
# NOTE: THIS DOCKERFILE IS GENERATED VIA "update.sh"
#
# PLEASE DO NOT EDIT IT DIRECTLY.
#
EOH
}

for version in "${versions[@]}"; do

    binUrl="https://franz.com/ftp/pub/acl$version/linuxamd64.64/acl$version-linux-x64.tbz2"
    binSha="$(curl -fsSL "$binUrl" | sha512sum | cut -d' ' -f1)"

    for v in \
        bullseye/{,slim} \
    ; do
        os="${v%%/*}"
        variant="${v#*/}"
        dir="$version/$v"

        mkdir -p "$dir"

        case "$os" in
            bullseye|buster|stretch)
                template="apt"
                if [ "$variant" = "slim" ]; then
                    from="debian:$os"
                else
                    from="buildpack-deps:$os"
                    cp install-quicklisp "$dir/install-quicklisp"
                fi
                cp alisp "$dir/alisp"
                cp mlisp "$dir/mlisp"
                cp allegro-express "$dir/allegro-express"
                cp docker-entrypoint.sh "$dir/docker-entrypoint.sh"
                ;;
        esac

        if [ -n "$variant" ]; then
            template="$template-$variant"
        fi

        template="Dockerfile-${template}.template"

        { generated_warning; cat "$template"; } > "$dir/Dockerfile"

        sed -ri \
            -e 's/^(ENV ALLEGRO_VERSION) .*/\1 '"$version"'/' \
            -e 's/^(ENV ALLEGRO_SHA512) .*/\1 '"$binSha"'/' \
            -e 's,^(FROM) .*,\1 '"$from"',' \
            "$dir/Dockerfile"
    done
done
